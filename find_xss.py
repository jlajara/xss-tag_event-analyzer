import argparse
import re
import json
from termcolor import colored
from prettytable import PrettyTable


def find_payload(filename, tags, events):
    '''
    Find suitable payloads inside db.json
    '''
    with open(filename) as json_file:
        data = json.load(json_file)

    result = []

    if events and not tags:
        for event in events:
            for tag in data[event]['tags']:
                result.append(tag)

    elif tags and not events:
        for key in data:
            for tag in data[key]['tags']:
                if tag['tag'] in tags:
                    result.append(tag)

    elif tags and events:
        events_find = []
        for key in data:
            if key in events:
                events_find.append(data[key])
        for event in events_find:
            for tag in event['tags']:
                if tag['tag'] in tags:
                    result.append(tag)

    if tags:
        result = check_multi_payload(result, tags)
    return result

def check_multi_payload(payloads, tags):
    '''
    Check if a payload with various tags can be exploited
    '''
    result = []
    for payload in payloads:
        tags_regex = re.findall(r'\<\w*[^\/]', payload['code'])
        allowed = True
        for i in tags_regex:
            i = i.replace("<", "")
            i = i.replace(">", "")
            i = i.replace(" ", "")
            if i not in tags:
                allowed = False
        if allowed:
            result.append(payload)
    return result


def prettyprint(payloads, output=""):
    '''
    Print the table and the output in a file
    '''
    browsers_colors = {'safari' : 'cyan',
                       'edge'   : 'blue',
                       'firefox': 'red',
                       'chrome' : 'white'}
    print("\nPayloads found:\n")

    table = PrettyTable(['Payload', 'Browser Compatibility'])
    table.align['Payload'] = "l"

    for payload in payloads:
        browsers = ''
        for browser in payload['browsers']:
            browsers += colored(browser, browsers_colors[browser]) + " "
        table.add_row([payload['code'], browsers])

    print(table)

    if output:
        file = open(output, 'w+')
        for payload in payloads:
            file.write(payload['code']+'\n')
        file.close()
        print("Payloads saved in: " + output)

def main():
    '''
    Main function
    '''
    parser = argparse.ArgumentParser(description='Find suitable XSS Payloads.')
    parser.add_argument('-f', '--file', help='file with the payloads', default='db.json')
    parser.add_argument('-t', '--tags', help='array with allowed tags', nargs='+')
    parser.add_argument('-e', '--events', help='array with allowed events', nargs='+')
    parser.add_argument('-o', '--output', help='output payload list')

    args = parser.parse_args()

    filename = args.file
    tags = args.tags
    events = args.events

    if events or tags:
        payloads = find_payload(filename, tags, events)
        prettyprint(payloads, args.output)

    else:
        parser.print_help()


if __name__ == "__main__":
    main()
